insert into  post (title, content) values ('title 1', 'content 1');
insert into  post (title, content) values ('title 2', 'content 2');
insert into  post (title, content) values ('title 3', 'content 3');
insert into  post (title, content) values ('title 4', 'content 4');
insert into  post (title, content) values ('title 5', 'content 5');

insert into  tag ( label)  values ('label 1');
insert into  tag ( label)  values ('label 2');
insert into  tag ( label)  values ('label 3');

insert into  post_tag ( post_id, tag_id)  values (1,1);
insert into  post_tag ( post_id, tag_id)  values (1,2);
insert into  post_tag ( post_id, tag_id)  values (1,3);
insert into  post_tag ( post_id, tag_id)  values (2,1);
insert into  post_tag ( post_id, tag_id)  values (3,3);
insert into  post_tag ( post_id, tag_id)  values (4,3);
insert into  post_tag ( post_id, tag_id)  values (5,2);
insert into  post_tag ( post_id, tag_id)  values (5,3);

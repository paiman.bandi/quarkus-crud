package org.paiman.resource;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.paiman.entity.Tag;
import org.paiman.service.TagService;
import org.paiman.dto.TagDTO;
import org.paiman.response.SuccessResponse;
import java.util.List;

import java.util.stream.Collectors;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagResource {
    @Inject
    TagService tagService;

    @GET
    @Path("/tags")
    public Response getAllTags() {
        List<TagDTO> response = tagService.getAllTags();
        return Response.ok().entity(new SuccessResponse(null, response)).build();
    }
}

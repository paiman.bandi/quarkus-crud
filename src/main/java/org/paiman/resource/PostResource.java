package org.paiman.resource;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.paiman.entity.Post;
import org.paiman.service.PostService;
import org.paiman.dto.PostDTO;
import org.paiman.dto.PostRequestDTO;
import org.paiman.response.ErrorResponse;
import org.paiman.response.SuccessResponse;
import java.util.List;

import java.util.stream.Collectors;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostResource {
    @Inject
    PostService postService;

    @GET
    @Path("/posts")
    public Response getAllPosts() {
        List<PostDTO> response = postService.getAllPosts();
        return Response.ok().entity(new SuccessResponse(null, response)).build();
    }

    @GET
    @Path("/posts/{postId}")
    public Response getPostById(@PathParam("postId") Long postId) {
        PostDTO post = postService.getPostById(postId);

        if (post != null) {
            return Response.ok(post).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse("Post not found with ID: " + postId)).build();
        }
    }

    @POST
    @Path("/posts")
    public Response createPost(PostRequestDTO pPostRequestDTO) {
        PostDTO createdPost = postService.createNewPost(pPostRequestDTO);
        return Response.status(Response.Status.CREATED).entity(new SuccessResponse("Succeed to create a new post", createdPost)).build();
    }
  
    @PUT
    @Path("/posts/{postId}")
    public Response updatePost(@PathParam("postId") Long postId, PostRequestDTO updatedPost) {
        PostDTO updatedDTO = postService.updatePost(postId, updatedPost);
        if(updatedDTO != null){
          return Response.ok().entity(new SuccessResponse("Succeed to update post with ID = " + postId, updatedDTO)).build();
        } else{
          return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse("Post not found with ID: " + postId)).build();
        }
    }

    @DELETE
    @Path("/posts/{postId}")
    public Response deletePost(@PathParam("postId") Long postId) {
        boolean deleted = postService.deletePost(postId);
        
        if (deleted) {
            return Response.ok().entity(new SuccessResponse("Succeed to delete post with ID = " + postId, null)).build();
        } else {
          return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse("Post not found with ID: " + postId)).build();
        }
    }
}

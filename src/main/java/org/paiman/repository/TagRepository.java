package org.paiman.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import org.paiman.entity.Tag;
import java.util.List;

@ApplicationScoped
public class TagRepository implements PanacheRepositoryBase<Tag, Long> {  
  @PersistenceContext
  EntityManager entityManager;

  public List<Tag> findAllTags() {
    return listAll();
  }
  
  public Tag findByLabel(String label) {
        return find("label", label).firstResult();
  }

  public Tag findOrCreateTag(String tagLabel) {
    Tag existingTag = findByLabel(tagLabel);
    if (existingTag != null) {
        return existingTag; 
    }

    Tag newTag = new Tag();
    newTag.setLabel(tagLabel);
    entityManager.persist(newTag); 
    return newTag;
  }
}


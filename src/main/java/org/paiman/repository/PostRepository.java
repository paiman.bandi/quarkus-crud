package org.paiman.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;

import org.paiman.entity.Post;
import java.util.List;

@ApplicationScoped
public class PostRepository implements PanacheRepositoryBase<Post, Long> {
   public List<Post> findAllPostsWithTags() {
        String jpql = "SELECT DISTINCT p FROM Post p JOIN FETCH p.tags";
        TypedQuery<Post> query = getEntityManager().createQuery(jpql, Post.class);
        return query.getResultList();
    }

    @Transactional
    public void create(Post post) {
        persist(post);
    }

    @Transactional
    public void updatePost(Post updatedPost) {
        // Post existingPost = findById(updatedPost.id); // Find the existing post by ID
        //
        // if (existingPost != null) {
        //     // Update the fields of the existing post with the new values
        //     existingPost.setTitle(updatedPost.getTitle());
        //     existingPost.setContent(updatedPost.getContent());
        //     existingPost.setContent(updatedPost.getContent());
        //
        //
            persist(updatedPost);
        // }
    }  
    
    @Transactional
    public void delete(Long id) {
      delete("id", id);
    }
}


package org.paiman.service;

import java.util.List;

import jakarta.transaction.Transactional;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.paiman.entity.Tag;
import org.paiman.repository.TagRepository;
import org.paiman.dto.TagDTO;
import java.util.stream.Collectors;

@ApplicationScoped
public class TagService {
  
  @Inject
  TagRepository tagRepository;
  
  public List<TagDTO> getAllTags() {
    List<Tag> tags = tagRepository.findAllTags();
    return tags.stream()
        .map(tag-> {
      TagDTO tagDTO = new TagDTO();
      tagDTO.setId(tag.getId());
      tagDTO.setLabel(tag.getLabel());
      return tagDTO;
        })
        .collect(Collectors.toList());
  }
}


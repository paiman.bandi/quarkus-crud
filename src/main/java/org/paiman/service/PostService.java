package org.paiman.service;

import java.util.List;

import jakarta.transaction.Transactional;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.paiman.entity.Post;
import org.paiman.entity.Tag;
import org.paiman.repository.PostRepository;
import org.paiman.repository.TagRepository;
import org.paiman.dto.PostDTO;
import java.util.stream.Collectors;
import org.paiman.dto.TagDTO;
import org.paiman.dto.PostRequestDTO;

@ApplicationScoped
public class PostService {
  
  @Inject
  PostRepository postRepository;

  @Inject
  TagRepository tagRepository;

  private PostDTO convertToPostDTO(Post post) {
            PostDTO postDTO = new PostDTO();
            postDTO.setId(post.getId());
            postDTO.setTitle(post.getTitle());
            postDTO.setContent(post.getContent());

            List<TagDTO> tagDTOs = post.getTags().stream()
                .map(tag -> {
                    TagDTO tagDTO = new TagDTO();
                    tagDTO.setId(tag.getId());
                    tagDTO.setLabel(tag.getLabel());
                    return tagDTO;
                })
                .collect(Collectors.toList());

            postDTO.setTags(tagDTOs);

            return postDTO;

  }
  
  public List<PostDTO> getAllPosts() {
    List<Post> posts = postRepository.findAllPostsWithTags();
    return posts.stream()
        .map(post -> {
      return convertToPostDTO(post);
        })
        .collect(Collectors.toList());
  }


  public PostDTO getPostById(Long postId) {
    Post post = postRepository.findById(postId);

    if (post != null) {
        return convertToPostDTO(post);
    } else {
        return null;
    }
  }
  
  @Transactional
  public PostDTO createNewPost(PostRequestDTO pPostRequestDTO) {
    Post post = new Post();
    post.setTitle(pPostRequestDTO.getTitle());
    post.setContent(pPostRequestDTO.getContent());

    List<Tag> lTag = pPostRequestDTO.getTags().stream()
        .map(tagLabel -> {
            Tag tag = tagRepository.findOrCreateTag(tagLabel); 
            return tag;
        })
        .collect(Collectors.toList());

    post.setTags(lTag);

    postRepository.persist(post);
    
    return convertToPostDTO(post);
  }

  @Transactional
  public PostDTO updatePost(Long postId, PostRequestDTO updatedPostDTO) {
    Post existingPost = postRepository.findById(postId);

    if (existingPost == null) {
      return null;
    }

    existingPost.setTitle(updatedPostDTO.getTitle());
    existingPost.setContent(updatedPostDTO.getContent());

    List<Tag> updatedTags = updatedPostDTO.getTags().stream()
        .map(tagLabel -> tagRepository.findOrCreateTag(tagLabel))
        .collect(Collectors.toList());

    existingPost.setTags(updatedTags);

    return convertToPostDTO(existingPost);
  }
  
  @Transactional
  public boolean deletePost(Long postId) {
      Post existingPost = postRepository.findById(postId);

      if (existingPost == null) {
          return false; 
      }

      postRepository.delete(postId);
      return true;
  }
}
